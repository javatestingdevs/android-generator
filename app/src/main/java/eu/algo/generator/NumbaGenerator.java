package eu.algo.generator;

import java.util.Random;

/**
 * Created by aleksandr.govorkov on 23-Mar-17.
 */

class NumbaGenerator {

    private Random generator = new Random();
    private int numba = 0;
    private int maxDigit = 10;

    public NumbaGenerator(int numba) {
        this.numba = numba;
    }

    public String getGeneratedNumba() {
        String generatedNumba = "";
        for (int i = 0; i < numba; i++) {
            int result = generator.nextInt(maxDigit);
            generatedNumba += Integer.toString(result);
        }
        return generatedNumba;
    }
}
