package eu.algo.generator;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import generator.R;

/**
 * Created by aleksandr.govorkov on 11-Oct-17.
 */

public class DigitNumbaActivity extends CustomActivity {

    private Answer answerObj = new Answer();
    private NumbaGenerator numbaGeneratorObj = new NumbaGenerator(Levels.getLevels().getCurrentNumba());
    private InterstitialAd mInterstitialAd = null;
    private TextView labelForAnswer;
    private TextView fieldOfNumba;
    private TextView abridgedRules1;
    private TextView abridgedRules2;
    private TextView arrowToAnswer;
    private TextView mXNumbaLbl;
    private TextView mInvalidUserSAnswer;
    private TextView mLevelLabel;
    private TextView mRateLabel;
    private TextView mSoughtSum;
    private EditText mUserSAnswerField;
    private Button mSubmitUserSAnswerBtn;
    private Button mGetAnswerBtn;
    private Button mNewNumbaBtn;
    private int adErrorCode;
    private boolean isShortRulesDismissed = false;
    private Resources resources;
    private final Animation in = new AlphaAnimation(0.0f, 1.0f);
    private final Animation out = new AlphaAnimation(1.0f, 0.0f);
    private float rateValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_20_digit_numba);

        mInterstitialAd = new InterstitialAd(this);
        fieldOfNumba = (TextView) findViewById(R.id.fieldOfNumba);
        fieldOfNumba.setPaintFlags(fieldOfNumba.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        abridgedRules1 = (TextView) findViewById(R.id.abridgedRules1);
        abridgedRules2 = (TextView) findViewById(R.id.abridgedRules2);
        arrowToAnswer = (TextView) findViewById(R.id.arrowToAnswer);
        labelForAnswer = (TextView) findViewById(R.id.fieldForAnswer);
        mXNumbaLbl = (TextView) findViewById(R.id.xNumbaLbl);
        mInvalidUserSAnswer = (TextView) findViewById(R.id.invalidUserSAnswer);
        mLevelLabel = (TextView) findViewById(R.id.intermediateLevelLabel);
        mSoughtSum = (TextView) findViewById(R.id.soughtSum);
        mUserSAnswerField = (EditText) findViewById(R.id.yourAnswerField);
        mSubmitUserSAnswerBtn = (Button) findViewById(R.id.submitUserSAnswerBtn);
        mGetAnswerBtn = (Button) findViewById(R.id.getAnswerBtn);
        mNewNumbaBtn = (Button) findViewById(R.id.newNumbaBtn);
        resources = getResources();


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // set the ad unit ID
                mInterstitialAd.setAdUnitId(getString(R.string.interstitialSd));
                requestNewInterstitial();
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        adErrorCode = errorCode;
                    }
                });

                if (6 == Levels.getLevels().getLevelCounter()) {
                    SpannableString ss = new SpannableString(resources.getString(R.string.last_level));
                    ClickableSpan cs = new ClickableSpan() {
                        @Override
                        public void onClick(View widget) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("market://details?id=eu.algo.generator"));
                            startActivity(intent);
                        }
                    };
                    ss.setSpan(cs, 25, 29, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mLevelLabel.setText(ss);
                    mLevelLabel.setMovementMethod(LinkMovementMethod.getInstance());
                    mLevelLabel.setHighlightColor(Color.TRANSPARENT);
                } else {
                    mLevelLabel.setText(resources.getString(R.string.intermediate_level, Levels.getLevels().getLevelCounter()));
                }
                mNewNumbaBtn.setVisibility(View.INVISIBLE);
                mInvalidUserSAnswer.setVisibility(View.INVISIBLE);
                mXNumbaLbl.setText(resources.getString(R.string.current_numba, Levels.getLevels().getCurrentNumba()));
                fieldOfNumba.setText(numbaGeneratorObj.getGeneratedNumba());
                mSoughtSum.setText(resources.getString(R.string.sought_sum_text, Levels.getLevels().getSoughtSum()));
                if (!isShortRulesDismissed) {
                    setShortRules();
                    isShortRulesDismissed = true;
                }
            }
        });
    }

    public void onClickSubmitAnswer(View view) {
        String userSAnswer = mUserSAnswerField.getText().toString();
        in.setDuration(500);
        out.setDuration(500);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                resetLabelForAnswer();
                dismissShortRules();
                sendGeneratedNumberToProcess();
            }
        });


        if (Integer.toString(answerObj.getAnswer()).equals(userSAnswer)) {
            //Start new level
            if (6 == Levels.getLevels().getLevelCounter()) {
                Intent startConclusion = new Intent(this, Conclusion.class);
                super.startActivity(startConclusion);
            } else {
                Levels.getLevels().setLevel(Levels.getLevels().getLevelCounter() + 1);
                Intent startNewLevel = new Intent(this, DigitNumbaActivity.class);
                super.startActivity(startNewLevel);
            }
        } else {
            mInvalidUserSAnswer.startAnimation(out);
            mInvalidUserSAnswer.setVisibility(View.VISIBLE);
            mInvalidUserSAnswer.startAnimation(in);
        }

    }

    public void onClickGetAnswer(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissShortRules();

                //Show interstitial ad
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        requestNewInterstitial();
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        adErrorCode = errorCode;
                    }
                });

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else if (AdRequest.ERROR_CODE_NETWORK_ERROR == adErrorCode) {
                    requestNewInterstitial();
                }

                try {
                    mInvalidUserSAnswer.setVisibility(View.INVISIBLE);
                    mUserSAnswerField.setEnabled(false);
                    mSubmitUserSAnswerBtn.setEnabled(false);
                    mGetAnswerBtn.setVisibility(View.INVISIBLE);
                    mNewNumbaBtn.setVisibility(View.VISIBLE);
                    sendGeneratedNumberToProcess();
                    //Element to show answer.
                    labelForAnswer.setText(resources.getString(R.string.valid_answer_text, Levels.getLevels().getSoughtSum(), answerObj.getAnswer()));
                } catch (NullPointerException error) {
                    labelForAnswer.setText(R.string.invalid_answer_text);
                } catch (final Exception e) {
                    Log.i(getTAG(), "Exception in thread.");
                }
            }
        });
    }

    public void onClickGenerateNewNumba(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                generateNumba();
            }
        });
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build(); // Production
        //AdRequest adRequest = new AdRequest.Builder().addTestDevice("A3E0CDF082C69ABA92936C5CC6CAD654").build(); // Test
        // Load ads into Interstitial Ad
        mInterstitialAd.loadAd(adRequest);
    }

    private void dismissShortRules() {
        arrowToAnswer.setText("");
        abridgedRules2.setText("");
        abridgedRules1.setText("");
    }

    private void setShortRules() {
        abridgedRules1.setText(resources.getString(R.string.short_rules_1, Levels.getLevels().getSoughtSum()));
        abridgedRules2.setText(R.string.short_rules_2);
        arrowToAnswer.setText(R.string.navigation_arrow_text);
    }
    
    private void sendGeneratedNumberToProcess() {
        answerObj.setGeneratedString(fieldOfNumba.getText().toString().split(""));
    }

    private void resetLabelForAnswer() {
        labelForAnswer.setText("");
    }

    private void generateNumba() {
        fieldOfNumba.setText(numbaGeneratorObj.getGeneratedNumba());
        mUserSAnswerField.setEnabled(true);
        mSubmitUserSAnswerBtn.setEnabled(true);
        mGetAnswerBtn.setVisibility(View.VISIBLE);
        mNewNumbaBtn.setVisibility(View.INVISIBLE);
        mUserSAnswerField.setHint(R.string.your_answer_text);
        resetLabelForAnswer();
    }

}
