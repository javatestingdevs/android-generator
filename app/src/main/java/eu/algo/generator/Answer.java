package eu.algo.generator;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Jenny on 19.04.2016.
 * copy-past from generator (java version)
 */


class Answer {
    private final String TAG = "Debug message";
    private String [] generatedString;

    public void setGeneratedString (String[] string) {
        //In 'generatedString' array first element is EMPTY SPACE. That why the app crashes. It cannot convert EMPTY SPACE to Integer.
        this.generatedString = string;

        //That's why we re-initialize 'generatedString' without having EMPTY SPACE.
        this.generatedString = Arrays.copyOfRange(this.generatedString, 1, Levels.getLevels().getCurrentNumba() + 1);
    }

    public Integer getAnswer() {
        return calculateAnswer(this.generatedString);
    }

    public Integer calculateAnswer(String[] stringOfNumba) {
        int amountOfSearchedValue = 0;

        ArrayList<Integer> generatedDigits = new ArrayList<Integer>();
        for (String s : stringOfNumba) {
            generatedDigits.add(Integer.parseInt(s));
        }

        try {
            for (int i = 0; i <= generatedDigits.size() - 1; i++) {
                int sum = (int) generatedDigits.toArray()[i];

                int j = i;
                while (sum <= Levels.getLevels().getSoughtSum()) {
                    if (j != generatedDigits.size() - 1) {
                        j++;

                        sum += (int) generatedDigits.toArray()[j];
                        if (sum == Levels.getLevels().getSoughtSum()) {
                            amountOfSearchedValue++;
                        }
                    } else {
                        break;
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Cannot calculate =/"); //For debugging.
        }
        return amountOfSearchedValue;
    }

}
