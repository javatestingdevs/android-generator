package eu.algo.generator;

import android.app.AlertDialog;
import android.content.DialogInterface;

import generator.R;

/**
 * Created by aleksandr.govorkov on 22-Mar-17.
 */

class Rules {

    Rules(AlertDialog.Builder builder) {
        builder.setTitle(R.string.rules_title_text)
                .setMessage(R.string.rules_text)
                .setCancelable(false)
                .setNegativeButton(R.string.rules_dismiss_btn_text,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
