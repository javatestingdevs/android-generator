package eu.algo.generator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;

import generator.R;

/**
 * Created by aleksandr.govorkov on 20-Oct-17.
 */

public class Conclusion extends CustomActivity {

    private int adErrorCode;
    private TextView mCongratulation;
    private TextView mLastQuestion;
    private Button mYesBtn;
    private Button mNoBtn;
    private AdView mAdView;
    private boolean isAlertButtonClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //First, set layout
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conclusion);

        //Second, assign elements from layout
        mCongratulation = (TextView) findViewById(R.id.congratulation);
        mLastQuestion = (TextView) findViewById(R.id.lastQuestion);
        mYesBtn = (Button) findViewById(R.id.yesBtn);
        mNoBtn = (Button) findViewById(R.id.noBtn);
        mAdView = (AdView) findViewById(R.id.conclusionAdView);

        setAdView(mAdView);
        loadBanner();

        mYesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        yesConclusionAlert();
                    }
                });
            }
        });

        mNoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        noConclusionAlert();
                    }
                });
            }
        });
    }

    private void noConclusionAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Conclusion.this);
        builder.setMessage(R.string.no_conclusion_text)
                .setCancelable(false)
                .setPositiveButton(R.string.agree_to_start_over,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int id) {
                                dialogInterface.dismiss();
                                startOver();
                            }
                        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void startOver() {
        Intent navigateToMain = new Intent(this, MainActivity.class);
        super.startActivity(navigateToMain);
    }

    private void yesConclusionAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Conclusion.this);
        builder.setMessage(R.string.yes_conclusion_text)
                .setCancelable(false)
                .setPositiveButton(R.string.check_apps,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int id) {
                                dialogInterface.dismiss();
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse("market://dev?id=7981974271066858968"));
                                if(intent.resolveActivity(getPackageManager()) != null) {
                                    startActivityForResult(intent, 0);
                                } else {
                                    intent.setData(Uri.parse("https://play.google.com/store/apps/dev?id=7981974271066858968"));
                                    startActivity(intent);
                                }
                            }
                        })
                .setNegativeButton(R.string.agree_to_start_over,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int id) {
                                dialogInterface.dismiss();
                                startOver();
                            }
                        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
