package eu.algo.generator;

/**
 * Created by aleksandr.govorkov on 13-Oct-17.
 */

public class Levels {

    private static Levels levels;
    private int levelCounter = 0;
    private int currentNumba = 0;
    private int soughtSum = 0;

    public static Levels getLevels() {
        if (levels == null) {
            levels = new Levels();
        }
        return levels;
    }

    public void setLevel(int levelNumber) {
        levelCounter = levelNumber;

        switch (levelNumber) {
            default:
                throw new IllegalArgumentException("No such level - " + levelNumber);
            case 1:
                currentNumba = 8;
                soughtSum = 10;
                break;
            case 2:
                currentNumba = 12;
                soughtSum = 13;
                break;
            case 3:
                currentNumba = 15;
                soughtSum = 14;
                break;
            case 4:
                currentNumba = 20;
                soughtSum = 16;
                break;
            case 5:
                currentNumba = 18;
                soughtSum = 19;
                break;
            case 6:
                currentNumba = 16;
                soughtSum = 20;
                break;
        }
    }

    public int getSoughtSum() {
        return soughtSum;
    }

    public int getCurrentNumba() {
        return currentNumba;
    }

    public void setLevelCounter(int levelCounter) {
        this.levelCounter = levelCounter;
    }

    public int getLevelCounter() {
        return levelCounter;
    }
}
