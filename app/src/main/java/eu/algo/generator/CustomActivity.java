package eu.algo.generator;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Created by aleksandr.govorkov on 12-Oct-17.
 */

public class CustomActivity extends AppCompatActivity {

    private final String TAG = "Debug message";
    private int adErrorCode;
    private boolean startOver = false;
    private AdView mAdView;

    @Override
    public void onBackPressed() {
        if (startOver) {
            Intent navigateToMain = new Intent(this, MainActivity.class);
            super.startActivity(navigateToMain);
        } else {
            Toast.makeText(this, "Press Back again to start over.",
                    Toast.LENGTH_SHORT).show();
            startOver = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startOver = false;
                }
            }, 3 * 1000);

        }
    }

    public String getTAG() {
        return TAG;
    }
    
    public void setAdView(AdView adView) {
        this.mAdView = adView;
    }

    private void requestNewBanner() {
        AdRequest adRequest = new AdRequest.Builder().build(); // Production
        //AdRequest adRequest = new AdRequest.Builder().addTestDevice("A3E0CDF082C69ABA92936C5CC6CAD654").build(); // Test
        // Load ads into Interstitial Ad
        mAdView.loadAd(adRequest);
    }

    public void loadBanner() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                requestNewBanner();
                mAdView.setAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        setAdErrorCode(errorCode);
                    }
                });
            }
        });
    }

    public void setAdErrorCode(int adErrorCode) {
        this.adErrorCode = adErrorCode;
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

}
