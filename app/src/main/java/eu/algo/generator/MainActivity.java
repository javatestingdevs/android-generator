package eu.algo.generator;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;

import generator.R;


public class MainActivity extends CustomActivity {

    private boolean exit = false;
    private Button mStartBtn;
    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //First, set layout
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Second, assign elements from layout
        TextView mVersion = (TextView) findViewById(R.id.aVersionLbl);
        mStartBtn = (Button) findViewById(R.id.startBtn);
        mAdView = (AdView) findViewById(R.id.startAdView);

        setAdView(mAdView);
        loadBanner();
    }

    public void onClickShowRules(View v) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                new Rules(builder);
            }
        });
    }

    public void onClickStartGame(View v) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int level = 5; // Set your level for debugging specific level: 5 (last) + 1
                Levels.getLevels().setLevelCounter(level); //Resetting levels
                startGame();
            }
        });
    }

    private void startGame() {
        Levels.getLevels().setLevel(Levels.getLevels().getLevelCounter()+1);
        Intent intent = new Intent(this, DigitNumbaActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            System.exit(0);
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }
    }

}
